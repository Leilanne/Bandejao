package com.example.leilane.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Leilane on 14/01/2017.
 */
public class UsuarioBD extends SQLiteOpenHelper {

    private static final String TAG = "sql";
    public static final String NOME_BANCO = "bandejao.db";
    public static final int VERSAO_BANCO = 1;
    public static final String ID = "_id";
    public static final String CONT = "cont";
    public static final String TABELA = "users";
    public static final String NOME = "nome";
    public static final String SOBRENOME = "sobrenome";
    public static final String USUARIO = "usuario";
    public static final String SENHA = "senha";
    public static final String DATANASC = "datanascimento";
    public static final String EMAIL = "email";
    public static final String CURSO = "curso";
    public static final String SQL_SELECT_USUARIO = "SELECT * FROM pessoa WHERE usuario = '%s'";
    SQLiteDatabase db;

    private Cursor cursor;

    public UsuarioBD(Context context) {
        super(context, NOME_BANCO, null, VERSAO_BANCO);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
      //  Log.d(TAG, "Criando a Tabela usuarios");

        String sql = "CREATE TABLE " + TABELA + "("
                + ID + " integer,"
                + CONT + " integer,"
                + NOME + " text,"
                + SOBRENOME + " text,"
                + USUARIO + " text,"
                + SENHA + " text,"
                + DATANASC + " text,"
                + EMAIL + " text,"
                + CURSO + " text"
                + ")";

        db.execSQL(sql);
        this.db = db;

      //  Log.d(TAG, "Tabela usuario Criada");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABELA);
        this.onCreate(db);
    }



    public String insereDados(Usuario usr) {

        ContentValues valores;
        long resultado;

        db = getWritableDatabase();
        valores = new ContentValues();


        String query = "select* from " + UsuarioBD.TABELA;
        Cursor cursor = db.rawQuery(query,null);
        int count = cursor.getCount();


        valores.put(UsuarioBD.ID, count);
        valores.put(UsuarioBD.NOME, usr.getNome());
        valores.put(UsuarioBD.SOBRENOME, usr.getSobrenome());
        valores.put(UsuarioBD.USUARIO, usr.getUsuario());
        valores.put(UsuarioBD.SENHA, usr.getSenha());
        valores.put(UsuarioBD.DATANASC, usr.getDatanasc());
        valores.put(UsuarioBD.EMAIL, usr.getEmail());
        valores.put(UsuarioBD.CURSO, usr.getEmail());

        resultado = db.insert(UsuarioBD.TABELA, null, valores);
        db.close();

        if (resultado == -1)
            return "Erro ao inserir registro";
        else
            return "Registro Inserido com sucesso";

    }

    public String procuraSenha(String usuario){

        db = getReadableDatabase();
        String query = "select usuario, senha from " + TABELA;
         //String query = "SELECT senha FROM users WHERE usuario = " + usuario ;
        Cursor cursor = db.rawQuery(query,null);
      // String s = cursor.getString(cursor.getColumnIndex("senha"));


        String login,senha;
        senha = "Usuário não encontrado";
        if(cursor.moveToFirst()){
            do{
                login=cursor.getString(0);

                if(login.equals(usuario)){

                    senha=cursor.getString(1);
                   break;


                }

               // senha = cursor.getString(cursor.getColumnIndex("senha"));
                //break;
            }
            while (cursor.moveToNext());
        }

        return senha;

    }


}
