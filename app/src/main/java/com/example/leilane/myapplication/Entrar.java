package com.example.leilane.myapplication;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.database.Cursor;
import android.widget.SimpleCursorAdapter;

public class Entrar extends AppCompatActivity {

    UsuarioBD crud = new UsuarioBD(getBaseContext());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrar);

        Button btEntrar = (Button)findViewById(R.id.bt_entrar); //Declara variável botão entrar
        Button btCadastrar = (Button)findViewById(R.id.bt_cadastrar); //Declara variável botão cadastrar

        btEntrar.setOnClickListener(onClickEntar()); //Chama classe para entar na tela do cardápio
        btCadastrar.setOnClickListener(onClickCadastrar()); //Chama classe para entrar na tela do cadastro
    }

    private View.OnClickListener onClickEntar() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    TextView tLogin = (TextView) findViewById(R.id.usuario);
                    TextView tSenha = (TextView) findViewById(R.id.senha);

                    String login = tLogin.getText().toString();
                    String senha = tSenha.getText().toString();

                    //String sen = crud.procuraSenha(login);
                   /* if(senha.equals(sen)){
                        Intent intent = new Intent(getContext(), Cardapio.class);
                        Bundle params = new Bundle();
                        startActivity(intent);
                    }*/
                    if(login.equals("leilanne") && senha.equals("senha") ){
                        Intent intent = new Intent(getContext(), Cardapio.class);
                        Bundle params = new Bundle();
                        startActivity(intent);
                    }
                    else if (login.equals("antonio") && senha.equals("busson") ){
                        Intent intent = new Intent(getContext(), Cardapio.class);
                        Bundle params = new Bundle();
                        startActivity(intent);
                    }
                    else if (login.equals("flavia") && senha.equals("senha123") ){
                        Intent intent = new Intent(getContext(), Cardapio.class);
                        Bundle params = new Bundle();
                        startActivity(intent);
                    }

                    else{
                        alert("Login e senha Incorretos. Por favor tente novamente");
                    }






                }
            };




    }

    private View.OnClickListener onClickCadastrar() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Cadastro.class);
                Bundle params = new Bundle();
                startActivity(intent);

            }

        };


    }


    private Context getContext() {
        return this;
    }

    private void alert (String s){
        Toast.makeText(this,s,Toast.LENGTH_SHORT).show();
    }
}
