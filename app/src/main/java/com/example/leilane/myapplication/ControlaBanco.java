package com.example.leilane.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;

/**
 * Created by Leilane on 14/01/2017.
 */

public class ControlaBanco {

    private SQLiteDatabase db;
    private UsuarioBD banco;

    public ControlaBanco(Context context) {
        banco = new UsuarioBD(context);}


   // public String insereDados(String nome, String sobrenome,String usuario, String senha,  String datanasc, String email, String curso) {
   public String insereDados(Usuario usr) {

       ContentValues valores;
        long resultado;

        db = banco.getWritableDatabase();
        valores = new ContentValues();


       String query = "select* from " + UsuarioBD.TABELA;
       Cursor cursor = db.rawQuery(query,null);
       int count = cursor.getCount();


       valores.put(UsuarioBD.ID, count);
        valores.put(UsuarioBD.NOME, usr.getNome());
        valores.put(UsuarioBD.SOBRENOME, usr.getSobrenome());
        valores.put(UsuarioBD.USUARIO, usr.getUsuario());
        valores.put(UsuarioBD.SENHA, usr.getSenha());
        valores.put(UsuarioBD.DATANASC, usr.getDatanasc());
        valores.put(UsuarioBD.EMAIL, usr.getEmail());
        valores.put(UsuarioBD.CURSO, usr.getEmail());

        resultado = db.insert(UsuarioBD.TABELA, null, valores);
        db.close();

        if (resultado == -1)
            return "Erro ao inserir registro";
        else
            return "Registro Inserido com sucesso";

    }

    public String procuraSenha(String usuario){

        db = banco.getReadableDatabase();
        String query = "select usuario, senha from " + UsuarioBD.TABELA;
       // String query = "SELECT * FROM users WHERE usuario = " + usuario;
        Cursor cursor = db.rawQuery(query,null);
        String login,senha;
        senha = "Usuário não encontrado";
        if(cursor.moveToFirst()){
            do{
                login=cursor.getString(0);

                if(login.equals(usuario)){

                    senha=cursor.getString(1);
                    break;

                }
            }
            while (cursor.moveToNext());
        }

        return senha;

    }

    public Cursor carregaDados(){
        Cursor cursor;
        String[] campos =  {banco.ID,banco.USUARIO};
        db = banco.getReadableDatabase();
        cursor = db.query(banco.TABELA, campos, null, null, null, null, null, null);

        if(cursor!=null){
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

    public int checkUser(Usuario us)
    {
        int id=-1;
        db= banco.getReadableDatabase();
        Cursor cursor=db.rawQuery("SELECT id FROM users WHERE usuario=? AND senha=?",new String[]{us.getUsuario(),us.getSenha()});
        if(cursor.getCount()>0) {
            cursor.moveToFirst();
            id=cursor.getInt(0);
            cursor.close();
        }
        return id;
    }




}


