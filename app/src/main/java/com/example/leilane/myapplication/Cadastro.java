package com.example.leilane.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Context;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;


public class Cadastro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        Button btCadastro = (Button) findViewById(R.id.bt_cadastro);
        assert btCadastro != null;
        btCadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UsuarioBD crud = new UsuarioBD(getBaseContext());

                TextView nome = (TextView)findViewById(R.id.nome);
                TextView sobrenome = (TextView)findViewById(R.id.sobrenome);
                TextView usuario = (TextView)findViewById(R.id.usuario);
                TextView senha = (TextView)findViewById(R.id.senha);
                TextView nascimento = (TextView)findViewById(R.id.nascimento);
                TextView email = (TextView)findViewById(R.id.email);
                TextView curso = (TextView)findViewById(R.id.curso);


                assert nome != null;
                String nomeString = nome.getText().toString();
                assert sobrenome != null;
                String sobrenomeString = sobrenome.getText().toString();
                assert usuario != null;
                String usuarioString = usuario.getText().toString();
                assert senha != null;
                String senhaString = senha.getText().toString();
                assert nascimento != null;
                String nascimentoString = nascimento.getText().toString();
                assert email != null;
                String emailString = email.getText().toString();
                assert curso != null;
                String cursoString = curso.getText().toString();
                String resultado;



                //resultado = crud.insereDados(new Usuario (nomeString,sobrenomeString,usuarioString,senhaString,nascimentoString,emailString,cursoString));

                Usuario us = new Usuario();
                us.setNome(nomeString);
                us.setSobrenome(sobrenomeString);
                us.setUsuario(usuarioString);
                us.setSenha(senhaString);
                us.setDatanasc(nascimentoString);
                us.setEmail(emailString);
                us.setCurso(cursoString);

                resultado = crud.insereDados(us);




                Toast.makeText(getApplicationContext(), resultado, Toast.LENGTH_LONG).show();


                // Intent intent = new Intent(getContext(), Cardapio.class);
                //Bundle params = new Bundle();
                //startActivity(intent);


            }
        });
    }
}