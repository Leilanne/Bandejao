package com.example.leilane.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;

import android.os.Bundle;

import android.view.Menu;
import android.view.View;

import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;

public class Cardapio extends Activity {

    private RatingBar ratingBar;
    private TextView txtRatingValor;
    private Button bt_comentar;

    private DatePicker datePicker;
    private Calendar calendario;
    private TextView dateView;
    private int ano, mes, dia;
    public  TextView valorCereal, valorProteico, valorSalada, valorAcompanhamento, valorLeguminosa, valorSob;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardapio);
        addListenerOnRatingBar();
        addListenerOnButton();

        dateView = (TextView) findViewById(R.id.dataSelecionada);
        valorCereal = (TextView)findViewById(R.id.valorCereal);
        valorProteico = (TextView)findViewById(R.id.valorProteico);
        valorSalada = (TextView)findViewById(R.id.valorSalada);
        valorAcompanhamento = (TextView)findViewById(R.id.valorAcompanhamento);
        valorLeguminosa = (TextView)findViewById(R.id.valorLeguminosa);
        valorSob = (TextView)findViewById(R.id.valorSobremesa);
        calendario = Calendar.getInstance();
        ano = calendario.get(Calendar.YEAR);
        mes = calendario.get(Calendar.MONTH);
        dia = calendario.get(Calendar.DAY_OF_MONTH);
        showDate(ano, mes+1, dia);
    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "Data Escolhida",
                Toast.LENGTH_SHORT)
                .show();
    }

    public void addListenerOnRatingBar(){
        ratingBar = (RatingBar)findViewById(R.id.ratingBar);
        txtRatingValor = (TextView) findViewById(R.id.txtRatingValor);

        ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean b) {
                txtRatingValor.setText(String.valueOf(rating));
            }
        });
    }

    public void addListenerOnButton(){
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        bt_comentar = (Button) findViewById(R.id.bt_comentar);

        bt_comentar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Cardapio.this,
                        String.valueOf(ratingBar.getRating()),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener,ano, mes, dia);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showDate(arg1, arg2+1, arg3);
                }
            };

    private void showDate(int ano, int mes, int dia) {
        dateView.setText(new StringBuilder().append(dia).append("/")
                .append(mes).append("/").append(ano));

        if (dia == 16 ){

            valorCereal.setText(new StringBuilder().append("Arroz"));
            valorProteico.setText(new StringBuilder().append("Bife de Fígado"));
            valorSalada.setText(new StringBuilder().append("Alface com Repolho"));
            valorAcompanhamento.setText(new StringBuilder().append("Farofa"));
            valorLeguminosa.setText(new StringBuilder().append("Feijão Preto"));
          //  valorSob.setText(new StringBuilder().append("Banana"));

        }
        else if (dia == 17){

                valorCereal.setText(new StringBuilder().append("Baião de Dois"));
                valorProteico.setText(new StringBuilder().append("Bisteca Suína"));
                valorSalada.setText(new StringBuilder().append("Alface e tomate"));
                valorAcompanhamento.setText(new StringBuilder().append("Espaguete à Primavera"));
                valorLeguminosa.setText(new StringBuilder().append("----"));
              //  valorSob.setText(new StringBuilder().append("Melância"));

            }
        else if (dia == 18){

            valorCereal.setText(new StringBuilder().append("Arroz Branco"));
            valorProteico.setText(new StringBuilder().append("Espetinho Misto"));
            valorSalada.setText(new StringBuilder().append("Alface com Repolho"));
            valorAcompanhamento.setText(new StringBuilder().append("Farofa"));
            valorLeguminosa.setText(new StringBuilder().append("Feijão Preto"));
         //   valorSob.setText(new StringBuilder().append("Banana"));

        }
        else if (dia == 19){

            valorCereal.setText(new StringBuilder().append("Arroz Temperado"));
            valorProteico.setText(new StringBuilder().append("Quibe Assado"));
            valorSalada.setText(new StringBuilder().append("Repolho e Milho Verde"));
            valorAcompanhamento.setText(new StringBuilder().append("Farofa"));
            valorLeguminosa.setText(new StringBuilder().append("Feijão"));
          //  valorSob.setText(new StringBuilder().append("Melão"));

        }
        else if (dia == 20){

            valorCereal.setText(new StringBuilder().append("Arroz Branco"));
            valorProteico.setText(new StringBuilder().append("Mocotó"));
            valorSalada.setText(new StringBuilder().append("Salada Verde"));
            valorAcompanhamento.setText(new StringBuilder().append("Farofa"));
            valorLeguminosa.setText(new StringBuilder().append("Feijão"));
          //  valorSob.setText(new StringBuilder().append("Laranja"));

        }
        else{

            Toast.makeText(getApplicationContext(), "Data Inválida", Toast.LENGTH_SHORT).show();
        }
        }

    }






