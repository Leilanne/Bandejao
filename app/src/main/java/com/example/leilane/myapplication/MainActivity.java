package com.example.leilane.myapplication;
        import android.content.Context;
        import android.content.Intent;
        import android.net.Uri;
        import  android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.google.android.gms.appindexing.Action;
        import com.google.android.gms.appindexing.AppIndex;
        import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btEntrar = (Button) findViewById(R.id.bt_entrar);
        btEntrar.setOnClickListener(onClickEntar());
    }


    private View.OnClickListener onClickEntar() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Entrar.class);
                Bundle params = new Bundle();
                startActivity(intent);

            }

        };


    }

    private Context getContext() {
        return this;
    }

}